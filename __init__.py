from pelican import signals
from . import age_in_days

def add_filter(pelican):
  """ Adds ``age_in_days`` filter to Pelican. """
  pelican.env.filters.update({'age_in_days': age_in_days.age_in_days})

def register():
  """ Registers the plugin when a generator initializes. """
  signals.generator_init.connect(add_filter)
