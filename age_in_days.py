from datetime import datetime

def age_in_days(value):
  """
  Return the number of days between now and the given datetime
  ``value``.
  """
  now = datetime.now(value.tzinfo)
  delta = now - value
  return delta.days
