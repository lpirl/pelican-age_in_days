This is basically `Kris Johnson's plugin <http://undefinedvalue.com/adding-a-jinja2-filter-with-a-pelican-plugin.html>`__
extracted from `his repository
<https://github.com/kristopherjohnson/undefinedvalue-pelican>`__ so it
is easier to be reused as a submodule.

--------

A trivial Pelican plugin which adds a Jinja filter to calculate the age (in
days) of a given date, e.g.:

.. code:: html+jinja

    Posted {% article.date|age_in_days %} days ago.

Also useful to show only the most recent posts on your landing page, e.g.:

.. code:: html+jinja

  …
  {% for article in articles
      if not article.date|age_in_days is greaterthan POSTS_ON_INDEX_MAX_AGE_IN_DAYS
         and not article.hidden and not article.status == 'draft' %}
    {% if loop.first %}
      <section>
      <h2>recent posts</h2>
    {% endif %}
    <article>
      <header>
        <a href="{{ article.url }}">{{ article.title }}</a>
      </header>
    </article>
    {% if loop.last %}
      </section>
    {% endif %}
  {% endfor %}
  …

Where ``POSTS_ON_INDEX_MAX_AGE_IN_DAYS`` needs to be defined in your
``pelicanconf.py`` but you can of course also hard-code the value instead.
